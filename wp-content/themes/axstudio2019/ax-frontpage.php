<?php
/*
Template Name: Front Page
*/

get_header();
?>
    <main>
        <section>
            <div class="container">
                <h1 class="title">Hello world</h1>
                <h2>Hello World</h2>
                <h3>Hello World</h3>
                <h4>Hello World</h4>
                <h5>Hello World</h5>
                <h6>Hello World</h6>
                <br/>
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                    ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                    voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                    culpa qui officia deserunt mollit anim id est laborum.</p>
                <br/>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                    aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                    culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
        </section>
        <section class="padding-default-0">
            <div class="container">
                <h1>Pills with dropdowns</h1>
                <hr/>
                <div class="nav nav-pills" id="pills-tab" role="tablist">

                    <a class="nav-link active" id="pills-one-tab" data-toggle="pill" href="#pills-one" role="tab"
                       aria-controls="pills-home" aria-selected="true">One</a>

                    <a class="nav-link" id="pills-two-tab" data-toggle="pill" href="#pills-two" role="tab"
                       aria-controls="pills-profile" aria-selected="false">Two</a>

                    <a class="nav-link" id="pills-three-tab" data-toggle="pill" href="#pills-three" role="tab"
                       aria-controls="pills-contact" aria-selected="false">Three</a>

                </div>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="pills-one" role="tabpanel"
                         aria-labelledby="pills-one-tab">
                        <h2>One</h2>
                    </div>
                    <div class="tab-pane fade" id="pills-two" role="tabpanel" aria-labelledby="pills-two-tab">
                        <h2>Two</h2>
                    </div>
                    <div class="tab-pane fade" id="pills-three" role="tabpanel" aria-labelledby="pills-three-tab">
                        <h2>Three</h2>
                    </div>
                </div>


            </div>
        </section>
        <section class="padding-default-0">
            <div class="container">
                <h1>Accordion</h1>
                <hr/>

                <div id="ax-accordion">
                    <div class="ax-accordion-item">
                        <div class="accordion-header" id="accordionOne">
                            <a href="#" class="btn btn-link display-block" data-toggle="collapse"
                               data-target="#collapseOne"
                               aria-expanded="true" aria-controls="collapseOne">
                                <h2>One</h2>
                            </a>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="accordionOne"
                             data-parent="#ax-accordion">
                            <div class="accordion-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad
                                squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck
                                quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it
                                squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica,
                                craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur
                                butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth
                                nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                    <div class="ax-accordion-item">
                        <div class="accordion-header" id="accordionTwo">
                            <a href="#" class="btn btn-link collapsed display-block" data-toggle="collapse"
                               data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <h2>Two</h2>
                            </a>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="accordionTwo"
                             data-parent="#ax-accordion">
                            <div class="accordion-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad
                                squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck
                                quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it
                                squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica,
                                craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur
                                butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth
                                nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                    <div class="ax-accordion-item">
                        <div class="accordion-header" id="accordionThree">
                            <h2 class="mb-0">
                                <a href="#" class="btn btn-link collapsed display-block" data-toggle="collapse"
                                   data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    <h2>Three</h2>
                                </a>
                            </h2>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="accordionThree"
                             data-parent="#ax-accordion">
                            <div class="accordion-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad
                                squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck
                                quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it
                                squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica,
                                craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur
                                butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth
                                nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="padding-default-0">
            <div class="container">
                <h1>Carousel</h1>
                <hr/>
                <div class="bd-example">
                    <div id="ax-carousel" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#ax-carousel" data-slide-to="0" class="active"></li>
                            <li data-target="#ax-carousel" data-slide-to="1"></li>
                            <li data-target="#ax-carousel" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="..." class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>First slide label</h5>
                                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img src="..." class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>Second slide label</h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img src="..." class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>Third slide label</h5>
                                    <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                                </div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#ax-carousel" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#ax-carousel" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </main>


<?php
get_footer();
