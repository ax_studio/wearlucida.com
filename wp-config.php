<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'lucida' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'm<RptU`1Q!bX4ap#[NVg[LtJ+Md>:UKb 1xs<$y21-JMkURx;CJr=;ySd#VY=KPU' );
define( 'SECURE_AUTH_KEY',  '-jiXX.?v<^;zT[WL_o]_=VF%/]!:+b|ms9V,1EgspJ*KfZS_t_[[=<es}yUi5wp.' );
define( 'LOGGED_IN_KEY',    '6 bSU_UqZf*vjJNn@M+7$=[4Z)<6}9LjP/7z@b^{-~!<T$Zs;@H7O2BlEr6Thwh*' );
define( 'NONCE_KEY',        'BShUWA=!l~U:U-y$vRL<Xqw7R6pWLC1q:ZfXJ3TUF4bm$Egp$ QEQ&T5GLzXwz9Y' );
define( 'AUTH_SALT',        '-j{1wcWQvYy 7&T(mZcd9Lr^2G&`xnK#WT>Y/@5WDXKH?PEh7>ONg[!FM*WB<NNO' );
define( 'SECURE_AUTH_SALT', 'HUTZr}>vnmqkQ;//78MzqLjT{lzbE>ymF=wn7=~rQ%]Cc#`{j~XoOL*Y2IzoPb&{' );
define( 'LOGGED_IN_SALT',   '6:tzsVWwb|XH<vBXUa]IQ<7|PY0;tlDnsV=~pRGXd@|Ctc$jZS(~TJ5OM$x{aYe!' );
define( 'NONCE_SALT',       '`MH8q$b0{Cy$_qmuc6Y^jCr#8X-..Rp$`Y,^/O $k0-;U-u~B~;![K0XVw$*JP3N' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'lucida_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
